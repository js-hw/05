function createNewUser() {
  const newUser = {};
  InputFirstName = prompt("What's your first name?", "Andrii");

  if (InputFirstName === null) {
    return
  } else {
    newUser.firstName = InputFirstName.trim();
  }
  while (true) {
    if (newUser.firstName.trim() === "") {
      newUser.firstName = prompt(
        "You've entered nothing. Please, enter your first name",
        `${""}`
      );
    } else {
      break;
    }
  }
  
  InputLastName = prompt("What's your last name?", "Kucher");
  if (InputLastName === null) {
    return
  } else {newUser.lastName = InputLastName.trim();
  }
  while (true) {
    if (newUser.lastName.trim() === "") {
      newUser.lastName = prompt(
        "You've entered nothing. Please, enter your last name",
        `${""}`
      );
    } else {
      break;
    }
  }

  newUser.getLogin = function () {
    return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
  };

  function message() {
    return `Welcome, ${newUser.lastName} ${
      newUser.firstName
    }! \nHere is your new login: ${newUser.getLogin()}`;
  }
  alert(message());
  console.log(message());
}
createNewUser();
